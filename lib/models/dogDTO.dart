class Dog {
  String id;
  String name;
  int age;
  String imageURL;
  String ownerId;
  String breedId;

  Dog({
    this.id,
    this.name,
    this.age,
    this.imageURL,
    this.ownerId,
    this.breedId,
  });

  Dog.fromJson(Map<String, dynamic> json) {
    this.id = json['id'];
    this.name = json['name'];
    this.age = json['age'];
    this.imageURL = json['imageURL'];
  }
}
