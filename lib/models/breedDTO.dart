class Breed {
  String id;
  String name;

  Breed({
    this.id,
    this.name,
  });

  Breed.fromJson(Map<String, dynamic> json) {
    this.id = json['id'];
    this.name = json['name'];
  }
}
