class Walker {
  String id;
  String name;
  String email;
  String uid;
  String identification;
  double rating;
  String imageURL;
  bool status;

  Walker({
    this.id,
    this.name,
    this.email,
    this.uid,
    this.identification,
    this.rating,
    this.imageURL,
    this.status,
  });

  Walker.fromJson(Map<String, dynamic> json) {
    this.id = json['id'];
    this.name = json['name'];
    this.email = json['email'];
    this.uid = json['uid'];
    this.identification = json['identification'];
    this.rating = json['rating'];
    this.imageURL = json['imageURL'];
    this.status = json['status'] == 0 ? false : true;
  }
}
