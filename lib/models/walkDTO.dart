class Walk {
  String id;
  DateTime date;
  double price;
  double distanceTraveled;
  int stepsWalked;
  double time;
  int status;
  String dogId;
  String ownerId;
  String walkerId;
  String neighborhoodGroupId;

  Walk({
    this.id,
    this.date,
    this.price,
    this.distanceTraveled,
    this.stepsWalked,
    this.time,
    this.status,
    this.dogId,
    this.ownerId,
    this.walkerId,
    this.neighborhoodGroupId,
  });

  Walk.fromJson(Map<String, dynamic> json) {
    this.id = json['id'];
    this.date = DateTime.fromMicrosecondsSinceEpoch(json['date']);
    this.price = json['price'];
    this.distanceTraveled = json['distanceTraveled'];
    this.stepsWalked = json['stepsWalked'];
    this.time = json['time'];
    this.status = json['status'];
  }
}
