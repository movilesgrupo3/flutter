class Owner {
  String id;
  String name;
  String email;
  String uid;
  String imageURL;
  String neighborhoodGroupId;

  Owner({
    this.id,
    this.name,
    this.email,
    this.uid,
    this.imageURL,
    this.neighborhoodGroupId,
  });

  Owner.fromJson(Map<String, dynamic> json) {
    this.id = json['id'];
    this.name = json['name'];
    this.email = json['email'];
    this.uid = json['uid'];
    this.imageURL = json['imageURL'];
  }
}
