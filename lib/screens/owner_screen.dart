import 'dart:async';

import 'package:canem_app/providers/auth_provider.dart';
import 'package:canem_app/providers/breed_provider.dart';
import 'package:canem_app/providers/owner_dogs_provider.dart';
import 'package:canem_app/providers/owner_provider.dart';
import 'package:canem_app/providers/owner_walks_provider.dart';
import 'package:canem_app/providers/walker_provider.dart';
import 'package:canem_app/screens/splash_screen.dart';
import 'package:canem_app/shared/session_state.dart';
import 'package:canem_app/shared/shared.dart';
import 'package:canem_app/utils/connectivity_utils.dart';
import 'package:convex_bottom_bar/convex_bottom_bar.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import "../widgets/owner/owner_historical.dart";
import "../widgets/owner/owner_pets.dart";
import "../widgets/owner/owner_profile.dart";
import "../widgets/owner/owner_search_walk.dart";

class OwnerScreen extends StatefulWidget {
  @override
  _OwnerScreenState createState() => _OwnerScreenState();
}

class _OwnerScreenState extends State<OwnerScreen> {
  dynamic defaultWidget = SplashScreen();
  var widgetLoaded = false;

  final List<TabItem<dynamic>> _items = [
    TabItem(icon: Icons.history, title: 'Historical'),
    TabItem(icon: Icons.search, title: 'Search Walk'),
    TabItem(icon: Icons.pets, title: 'My Pets'),
    TabItem(icon: Icons.person, title: 'Profile'),
  ];

  final List<Widget> _screens = [
    OwnerHistorical(),
    OwnerSearchWalk(),
    OwnerPets(),
    OwnerProfile(),
  ];

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    if (!widgetLoaded) {
      Timer.periodic(Duration(seconds: 1), (timer) {
        ConnectivityUtils.executeFunctionIfInternet(() {
          loadConfiguration();
          timer.cancel();
        }, () {
          setState(() {
            defaultWidget = Scaffold(
              body: Center(
                child: Text(
                    'Your device must be connected to Internet in order to access to the app'),
              ),
            );
          });
        });
      });
    }
    return defaultWidget;
  }

  void loadConfiguration() async {
    if (SessionState.firstLogin) {
      await Shared.genericSetup();
      final ownerProvider = Provider.of<OwnerProvider>(context, listen: false);
      await ownerProvider
          .fetchOwnerUID(Provider.of<Auth>(context, listen: false).userId);
      SessionState.user = ownerProvider.owner;
      final ownerWalksProvider =
          Provider.of<OwnerWalksProvider>(context, listen: false);
      await ownerWalksProvider.fetchWalksStatus(1);
      if (ownerWalksProvider.currentWalk != null) {
        SessionState.currentWalk = ownerWalksProvider.currentWalk;
        final walkerProvider =
            Provider.of<WalkerProvider>(context, listen: false);
        await walkerProvider.fetchWalker(SessionState.currentWalk.walkerId);
        SessionState.currentWalker = walkerProvider.walker;
        SessionState.walkState = OwnerSearchWalkState.LIVE_WALK;
      }
      await ownerWalksProvider.fetchWalks();
      await Provider.of<BreedsProvider>(context, listen: false).fetchBreeds();
      await Provider.of<OwnerDogsProvider>(context, listen: false)
          .fetchOwnerDogs();
      SessionState.firstLogin = false;
      setState(() {
        defaultWidget = DefaultTabController(
          initialIndex: 2,
          length: _items.length,
          child: Scaffold(
            appBar: AppBar(
              leading: Container(),
              title: Text("Owner"),
            ),
            body: SafeArea(
              child: TabBarView(
                children: _screens,
              ),
            ),
            bottomNavigationBar: ConvexAppBar(
              backgroundColor: Theme.of(context).primaryColor,
              items: _items,
              onTap: null,
            ),
          ),
        );
      });
    }
  }
}
