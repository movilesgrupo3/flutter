import 'package:canem_app/utils/connectivity_utils.dart';
import 'package:dropdown_formfield/dropdown_formfield.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../models/http_exception.dart';
import '../providers/auth_provider.dart';

enum AuthMode { Signup, Login }

class AuthScreen extends StatelessWidget {
  static const routeName = '/auth';

  @override
  Widget build(BuildContext context) {
    final deviceSize = MediaQuery.of(context).size;

    return Scaffold(
      body: SafeArea(
        child: Stack(
          children: <Widget>[
            Container(
              decoration: BoxDecoration(
                gradient: LinearGradient(
                  colors: [
                    Colors.deepPurple,
                    Colors.deepPurpleAccent.shade200,
                  ],
                  begin: Alignment.topLeft,
                  end: Alignment.bottomRight,
                  stops: [0, 1],
                ),
              ),
            ),
            SingleChildScrollView(
              child: Container(
                height: deviceSize.height,
                width: deviceSize.width,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Flexible(
                      child: Text(
                        "CANEM",
                        style: TextStyle(
                          color: Colors.white,
                          fontWeight: FontWeight.w900,
                          fontSize: 50,
                          letterSpacing: 3,
                        ),
                      ),
                    ),
                    Flexible(
                      child: SizedBox(
                        height: 10,
                      ),
                    ),
                    Flexible(
                      child: Image(
                        image: AssetImage('assets/images/canem_logo.png'),
                        height: deviceSize.height * 0.2,
                        width: deviceSize.height * 0.2,
                      ),
                    ),
                    AuthCard(),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class AuthCard extends StatefulWidget {
  const AuthCard({
    Key key,
  }) : super(key: key);

  @override
  _AuthCardState createState() => _AuthCardState();
}

class _AuthCardState extends State<AuthCard> {
  final GlobalKey<FormState> _formKey = GlobalKey();
  AuthMode _authMode = AuthMode.Login;
  Map<String, String> _authData = {
    'name': '',
    'email': '',
    'password': '',
    'role': ''
  };
  var _isLoading = false;
  final _passwordController = TextEditingController();

  void _showErrorDialog(String message) {
    showDialog(
      context: context,
      builder: (ctx) => AlertDialog(
        title: Text("An error occurred"),
        content: Text(message),
        actions: [
          FlatButton(
            onPressed: () => Navigator.of(context).pop(),
            child: Text("OK"),
          ),
        ],
      ),
    );
  }

  Future<void> _submit() async {
    await ConnectivityUtils.executeFunctionIfInternet(() async {
      if (!_formKey.currentState.validate()) {
        // Invalid Form
        return;
      }
      _formKey.currentState.save();
      setState(() {
        _isLoading = true;
      });
      try {
        if (_authMode == AuthMode.Login) {
          // Log user in
          await Provider.of<Auth>(context, listen: false).signIn(
            _authData["email"],
            _authData["password"],
          );
        } else {
          // Sign user up
          await Provider.of<Auth>(context, listen: false).signUp(
            _authData["name"],
            _authData["email"],
            _authData["password"],
            _authData["role"],
          );
        }
      } on HttpException catch (error) {
        // Firebase Auth error
        var errorMessage = "Authentication failed";
        if (error.toString().contains("EMAIL_EXISTS")) {
          errorMessage = "This email address is already in use.";
        } else if (error.toString().contains("INVALID_EMAIL")) {
          errorMessage = "This is not a email address.";
        } else if (error.toString().contains("WEAK_PASSWORD")) {
          errorMessage = "This password is too weak.";
        } else if (error.toString().contains("EMAIL_NOT_FOUND")) {
          errorMessage = "Could not find an user with that email.";
        } else if (error.toString().contains("INVALID_PASSWORD")) {
          errorMessage = "Invalid password.";
        }
        _showErrorDialog(errorMessage);
      } catch (error) {
        // Generic error
        const errorMessage = "Could not authenticate. Please try again later.";
        _showErrorDialog(errorMessage);
      }
      setState(() {
        _isLoading = false;
      });
    }, () {
      ConnectivityUtils.showConnectivityAlert(ConnectivityUtils.defaultTitle,
          ConnectivityUtils.defaultContent, context);
    });
  }

  void _switchAuthMode() {
    if (_authMode == AuthMode.Login) {
      setState(() {
        _authMode = AuthMode.Signup;
      });
    } else {
      setState(() {
        _authMode = AuthMode.Login;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    final deviceSize = MediaQuery.of(context).size;
    return Card(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(10.0),
      ),
      elevation: 8.0,
      child: Container(
        height: _authMode == AuthMode.Signup ? 380 : 260,
        constraints:
            BoxConstraints(minHeight: _authMode == AuthMode.Signup ? 380 : 260),
        width: deviceSize.width * 0.80,
        padding: EdgeInsets.all(16.0),
        child: Form(
          key: _formKey,
          child: SingleChildScrollView(
            child: Column(
              children: <Widget>[
                if (_authMode == AuthMode.Signup)
                  TextFormField(
                    enabled: _authMode == AuthMode.Signup,
                    decoration: InputDecoration(labelText: 'Name'),
                    validator: (value) {
                      if (value.isEmpty ||
                          value.length < 5 ||
                          !value.contains("")) {
                        return 'Enter a valid name.';
                      }
                      return null;
                    },
                    onSaved: (value) {
                      _authData['name'] = value;
                    },
                  ),
                TextFormField(
                  decoration: InputDecoration(labelText: 'Email'),
                  keyboardType: TextInputType.emailAddress,
                  validator: (value) {
                    Pattern pattern =
                        r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
                    RegExp regex = new RegExp(pattern);
                    if (value.isEmpty || !regex.hasMatch(value))
                      return 'Enter a valid Email.';
                    else
                      return null;
                  },
                  onSaved: (value) {
                    _authData['email'] = value;
                  },
                ),
                TextFormField(
                  decoration: InputDecoration(labelText: 'Password'),
                  obscureText: true,
                  controller: _passwordController,
                  validator: (value) {
                    if (value.isEmpty || value.length < 5) {
                      return 'Password is too short.';
                    }
                    return null;
                  },
                  onSaved: (value) {
                    _authData['password'] = value;
                  },
                ),
                if (_authMode == AuthMode.Signup)
                  DropDownFormField(
                    validator: (value) {
                      if (value == null || value.isEmpty || value == '') {
                        return 'Choose a role.';
                      }
                      return null;
                    },
                    titleText: 'Role',
                    value: _authData['role'],
                    onSaved: (value) {
                      setState(() {
                        _authData['role'] = value;
                      });
                    },
                    onChanged: (value) {
                      setState(() {
                        _authData['role'] = value;
                      });
                    },
                    dataSource: [
                      {
                        "display": "Owner",
                        "value": "Owner",
                      },
                      {
                        "display": "Walker",
                        "value": "Walker",
                      },
                    ],
                    textField: 'display',
                    valueField: 'value',
                  ),
                SizedBox(
                  height: 20,
                ),
                if (_isLoading)
                  CircularProgressIndicator(
                    valueColor:
                        AlwaysStoppedAnimation<Color>(Colors.deepPurple),
                  )
                else
                  RaisedButton(
                    child:
                        Text(_authMode == AuthMode.Login ? 'LOGIN' : 'SIGN UP'),
                    onPressed: _submit,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(30),
                    ),
                    padding:
                        EdgeInsets.symmetric(horizontal: 30.0, vertical: 8.0),
                    color: Theme.of(context).primaryColor,
                    textColor: Theme.of(context).primaryTextTheme.button.color,
                  ),
                FlatButton(
                  child: Text(
                      '${_authMode == AuthMode.Login ? 'SIGNUP' : 'LOGIN'} INSTEAD'),
                  onPressed: _switchAuthMode,
                  padding: EdgeInsets.symmetric(horizontal: 30.0, vertical: 4),
                  materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
                  textColor: Theme.of(context).primaryColor,
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
