import 'dart:async';

import 'package:canem_app/providers/auth_provider.dart';
import 'package:canem_app/providers/walker_provider.dart';
import 'package:canem_app/providers/walker_walks_provider.dart';
import 'package:canem_app/screens/splash_screen.dart';
import 'package:canem_app/shared/session_state.dart';
import 'package:canem_app/shared/shared.dart';
import 'package:canem_app/utils/connectivity_utils.dart';
import 'package:convex_bottom_bar/convex_bottom_bar.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import "../widgets/walker/walker_historical.dart";
import "../widgets/walker/walker_profile.dart";
import "../widgets/walker/walker_search_walk.dart";

class WalkerScreen extends StatefulWidget {
  @override
  _WalkerScreenState createState() => _WalkerScreenState();
}

class _WalkerScreenState extends State<WalkerScreen> {
  dynamic defaultWidget = SplashScreen();
  var widgetLoaded = false;

  final List<TabItem<dynamic>> _items = [
    TabItem(icon: Icons.history, title: 'Historical'),
    TabItem(icon: Icons.search, title: 'Search Walk'),
    TabItem(icon: Icons.person, title: 'Profile'),
  ];

  final List<Widget> _screens = [
    WalkerHistorical(),
    WalkerSearchWalk(),
    WalkerProfile(),
  ];

  @override
  Widget build(BuildContext context) {
    if (!widgetLoaded) {
      Timer.periodic(Duration(seconds: 1), (timer) {
        ConnectivityUtils.executeFunctionIfInternet(() {
          loadConfiguration();
          timer.cancel();
        }, () {
          setState(() {
            defaultWidget = Scaffold(
              body: Center(
                child: Text(
                    'Your device must be connected to Internet in order to access to the app'),
              ),
            );
          });
        });
      });
    }
    return defaultWidget;
  }

  void loadConfiguration() async {
    if (SessionState.firstLogin) {
      await Shared.genericSetup();
      final walkerProvider =
          Provider.of<WalkerProvider>(context, listen: false);
      await walkerProvider
          .fetchWalkerUID(Provider.of<Auth>(context, listen: false).userId);
      SessionState.user = walkerProvider.walker;
      final walkerWalksProvider =
          Provider.of<WalkerWalksProvider>(context, listen: false);
      await walkerWalksProvider.fetchWalksStatus(0);
      if (walkerWalksProvider.currentWalk != null) {
        SessionState.currentWalk = walkerWalksProvider.currentWalk;
        final walkerProvider =
            Provider.of<WalkerProvider>(context, listen: false);
        await walkerProvider.fetchWalker(SessionState.currentWalk.walkerId);
        SessionState.currentWalker = walkerProvider.walker;
        SessionState.walkState = WalkerSearchWalkState.SEARCHING_WIDGET;
      } else {
        await walkerWalksProvider.fetchWalksStatus(1);
        if (walkerWalksProvider.currentWalk != null) {
          SessionState.currentWalk = walkerWalksProvider.currentWalk;
          final walkerProvider =
              Provider.of<WalkerProvider>(context, listen: false);
          await walkerProvider.fetchWalker(SessionState.currentWalk.walkerId);
          SessionState.currentWalker = walkerProvider.walker;
          SessionState.walkState = WalkerSearchWalkState.LIVE_WALK_WIDGET;
        }
      }
      await walkerWalksProvider.fetchWalks();
      SessionState.firstLogin = false;
      setState(() {
        defaultWidget = DefaultTabController(
          initialIndex: 1,
          length: _items.length,
          child: Scaffold(
            appBar: AppBar(
              title: Text("Walker"),
            ),
            body: SafeArea(
              child: TabBarView(
                children: _screens,
              ),
            ),
            bottomNavigationBar: ConvexAppBar(
              backgroundColor: Theme.of(context).primaryColor,
              items: _items,
              onTap: null,
            ),
          ),
        );
      });
    }
  }
}
