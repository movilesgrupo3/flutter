library loading;

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class Loading {
  static Widget showLoadingScreen(String message) {
    return Column(children: [
      Container(
          height: 400,
          width: 400,
          alignment: Alignment.center,
          child: CircularProgressIndicator(
            valueColor: AlwaysStoppedAnimation<Color>(Colors.deepPurple),
          )),
      Text(message, style: TextStyle(fontSize: 20))
    ]);
  }
}
