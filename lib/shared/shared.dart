library shared;

import 'package:canem_app/localDB/database_creator.dart';
import 'package:firebase_core/firebase_core.dart';

class Shared {
  static String backIP = "https://canem-backend.herokuapp.com/api/";

  static Future<void> genericSetup() async {
    await DatabaseCreator().initDatabase();
    await Firebase.initializeApp();
  }

  static List<double> parseCoordinates(String coordinates) {
    var coordinatesString = coordinates.split(";");
    return [
      double.parse(coordinatesString[0]),
      double.parse(coordinatesString[1])
    ];
  }

  static String parseCoordinatesToString(double lat, double long) {
    return lat.toString() + ";" + long.toString();
  }
}
