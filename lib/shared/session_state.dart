library session_state;

import 'package:canem_app/models/walkDTO.dart';
import 'package:canem_app/models/walkerDTO.dart';

class SessionState {
  static dynamic user;
  static bool reminder = false;
  static bool firstLogin = true;
  static int walkState = 0;
  static Walk currentWalk;
  static Walker currentWalker;
  static List<Walker> fetchedWalkers;

  static logout() {
    user = null;
    reminder = false;
    firstLogin = true;
    walkState = 0;
    currentWalk = null;
    currentWalker = null;
    fetchedWalkers = [];
  }
}
