import 'dart:async';

import 'package:canem_app/providers/breed_provider.dart';
import 'package:canem_app/providers/owner_dogs_provider.dart';
import 'package:canem_app/providers/owner_provider.dart';
import 'package:canem_app/providers/walk_provider.dart';
import 'package:canem_app/providers/walker_provider.dart';
import 'package:canem_app/providers/walker_walks_provider.dart';
import 'package:firebase_crashlytics/firebase_crashlytics.dart';
import 'package:flutter/material.dart';
import 'package:flutter_config/flutter_config.dart';
import 'package:provider/provider.dart';

import './providers/owner_walks_provider.dart';
import './screens/auth_screen.dart';
import './screens/owner_screen.dart';
import './screens/splash_screen.dart';
import './screens/walker_screen.dart';
import 'providers/auth_provider.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  FlutterConfig.loadEnvVariables();
  runZonedGuarded(() {
    runApp(MyApp());
  }, (error, stackTrace) {
    FirebaseCrashlytics.instance.recordError(error, stackTrace);
  });
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider(builder: (ctx) => Auth()),
        ChangeNotifierProxyProvider<Auth, BreedsProvider>(
          builder: (ctx, auth, prevBreedsProvider) => BreedsProvider(
              prevBreedsProvider == null ? [] : prevBreedsProvider.breeds),
        ),
        ChangeNotifierProxyProvider<Auth, OwnerDogsProvider>(
          builder: (ctx, auth, prevOwnerDogsProvider) => OwnerDogsProvider(
              auth.token,
              prevOwnerDogsProvider == null ? [] : prevOwnerDogsProvider.dogs),
        ),
        ChangeNotifierProxyProvider<Auth, OwnerProvider>(
          builder: (ctx, auth, prevOwnerProvider) => OwnerProvider(
              prevOwnerProvider == null ? null : prevOwnerProvider.owner),
        ),
        ChangeNotifierProxyProvider<Auth, OwnerWalksProvider>(
          builder: (ctx, auth, prevOwnerWalksProvider) => OwnerWalksProvider(
              auth.token,
              prevOwnerWalksProvider == null
                  ? []
                  : prevOwnerWalksProvider.walks),
        ),
        ChangeNotifierProxyProvider<Auth, WalkProvider>(
          builder: (ctx, auth, prevWalkProvider) => WalkProvider(
              prevWalkProvider == null ? null : prevWalkProvider.walk),
        ),
        ChangeNotifierProxyProvider<Auth, WalkerProvider>(
          builder: (ctx, auth, prevWalkerProvider) => WalkerProvider(
              auth.token,
              prevWalkerProvider == null ? [] : prevWalkerProvider.walkers,
              prevWalkerProvider == null ? null : prevWalkerProvider.walker),
        ),
        ChangeNotifierProxyProvider<Auth, WalkerWalksProvider>(
          builder: (ctx, auth, prevOwnerWalksProvider) => WalkerWalksProvider(
              auth.token,
              prevOwnerWalksProvider == null
                  ? []
                  : prevOwnerWalksProvider.walks),
        ),
      ],
      child: Consumer<Auth>(
        builder: (ctx, auth, _) => MaterialApp(
          title: 'Canem App',
          theme: ThemeData(
            primarySwatch: Colors.deepPurple,
            accentColor: Colors.deepOrange,
            fontFamily: "Lato",
          ),
          routes: {
            "/": (ctx) => auth.isAuthenticated
                ? auth.role == "Owner"
                    ? OwnerScreen()
                    : WalkerScreen()
                : FutureBuilder(
                    future: auth.tryAutoLogin(),
                    builder: (ctx, authResultSnapshot) =>
                        authResultSnapshot.connectionState ==
                                ConnectionState.waiting
                            ? SplashScreen()
                            : AuthScreen()),
          },
        ),
      ),
    );
  }
}
