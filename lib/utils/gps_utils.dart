library gps_utils;

import 'package:geolocator/geolocator.dart';

class GPSUtils {
  static Future<dynamic> executeFunctionIfGPS(
      Function gps, Function noGps) async {
    return Geolocator.isLocationServiceEnabled().then((serviceEnabled) {
      if (!serviceEnabled) {
        return Geolocator.requestPermission().then((value) => noGps());
      } else {
        return Geolocator.checkPermission().then((value) {
          if (value == LocationPermission.denied ||
              value == LocationPermission.deniedForever) {
            return Geolocator.requestPermission().then((value) {
              if (value == LocationPermission.denied ||
                  value == LocationPermission.deniedForever) {
                return noGps();
              } else {
                return gps();
              }
            });
          } else {
            return gps();
          }
        });
      }
    });
  }
}
