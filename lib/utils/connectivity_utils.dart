library connectivity_utils;

import 'package:connectivity/connectivity.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class ConnectivityUtils {
  static String defaultTitle =
      'Your device must be connected to Internet in order to access this functionality';
  static String defaultContent =
      'Please, check your connection to Internet and try again later';
  static ConnectivityResult connectivityResult;

  static Future<bool> checkConnectivity() async {
    try {
      return Connectivity().checkConnectivity().then((value) {
        if ((value == ConnectivityResult.mobile) ||
            (value == ConnectivityResult.wifi)) {
          return true;
        }
        return false;
      });
    } catch (exception) {
      return Future<bool>(() => false);
    }
  }

  static void showConnectivityAlert(
      String title, String content, BuildContext context) {
    showDialog(
        context: context,
        builder: (context) => AlertDialog(
              title: Text(title),
              content: Text(content),
              actions: <Widget>[
                FlatButton(
                    onPressed: () {
                      Navigator.of(context).pop('OK');
                    },
                    child: Text('OK'))
              ],
            ));
  }

  static Future<dynamic> executeFunctionIfInternet(
      Function internet, Function noInternet) async {
    return checkConnectivity().then((value) {
      if (value) {
        return internet();
      } else {
        return noInternet();
      }
    });
  }
}
