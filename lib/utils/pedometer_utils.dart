import 'package:flutter/cupertino.dart';
import 'package:pedometer/pedometer.dart';

class PedometerUtils {
  static const NO_PEDOMETER_AVAILABLE = 'Step Count sensor not available';

  int initialSteps = -1, currentSteps = -1;
  Stream<StepCount> _stepCountStream;
  String steps = 'No steps registered';
  State state;

  void setUpPedometer(State state) async {
    _stepCountStream = Pedometer.stepCountStream;
    _stepCountStream.listen(onStepCount).onError(onStepCountError);
    this.state = state;
  }

  void onStepCount(StepCount event) {
    state.setState(() {
      if (initialSteps == -1) {
        initialSteps = event.steps;
      }
      currentSteps = event.steps - initialSteps;
      steps = currentSteps.toString() + ' steps';
    });
  }

  void onStepCountError(error) {
    state.setState(() {
      steps = NO_PEDOMETER_AVAILABLE;
    });
  }

  void onStepCountRestart() {
    state.setState(() {
      if (!identical(steps, NO_PEDOMETER_AVAILABLE)) {
        initialSteps = currentSteps + initialSteps;
        currentSteps = 0;
        steps = currentSteps.toString() + ' steps';
      }
    });
  }
}
