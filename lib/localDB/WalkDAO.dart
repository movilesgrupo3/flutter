import 'package:canem_app/models/walkDTO.dart';
import 'package:sqflite/sqflite.dart';

import 'database_creator.dart';

class WalkDAO {
  static const walkTable = 'walkTable';

  Future<void> createWalkTable(Database db) async {
    final query = '''CREATE TABLE $walkTable
    (
      id TEXT PRIMARY KEY,
      date DATETIME,
      price REAL,
      distanceTraveled REAL,
      stepsWalked INTEGER,
      time REAL,
      status INTEGER
    );''';
    await db.execute(query);
  }

  static Future<void> deleteAllWalkTable() async {
    final query = '''DELETE FROM $walkTable;''';
    await db.execute(query);
  }

  static Future<List<Walk>> getAllWalks() async {
    try {
      final sql = '''SELECT * FROM $walkTable  ''';
      final data = await db.rawQuery(sql);
        List<Walk> walks = [];
        for (final node in data) {
          final walk = Walk.fromJson(node);
          walks.add(walk);
        }
        return walks;
    } catch (exception) {
      return [];
    }
  }

  static Future<void> addWalk(Walk walk) async {
    try {
      final sql = '''INSERT INTO $walkTable
    (
      id,
      date,
      price,
      distanceTraveled,
      stepsWalked,
      time,
      status
    )
    VALUES (?,?,?,?,?,?,?);''';
      List<dynamic> params = [
        walk.id,
        walk.date.millisecondsSinceEpoch,
        walk.price,
        walk.distanceTraveled,
        walk.stepsWalked,
        walk.time,
        walk.status
      ];
      await db.rawInsert(sql, params);
    } catch (exception) {}
  }
}
