import 'package:canem_app/models/breedDTO.dart';
import 'package:sqflite/sqflite.dart';

import 'database_creator.dart';

class BreedDAO {
  static const breedTable = 'breedTable';

  Future<void> createBreedTable(Database db) async {
    final query = '''CREATE TABLE $breedTable
    (
      id TEXT PRIMARY KEY,
      name TEXT
    );''';
    await db.execute(query);
  }

  static Future<void> deleteAllBreedTable() async {
    final query = '''DELETE FROM $breedTable;''';
    await db.execute(query);
  }

  static Future<List<Breed>> getAllBreeds() async {
    try {
      final sql = '''SELECT * FROM $breedTable  ''';
      final data = await db.rawQuery(sql);
      List<Breed> breeds = [];
      for (final node in data) {
        final breed = Breed.fromJson(node);
        breeds.add(breed);
      }
      return breeds;
    } catch (exception) {
      return [];
    }
  }

  static Future<void> addBreed(Breed breed) async {
    try {
      final sql = '''INSERT INTO $breedTable
    (
      id,
      name
    )
    VALUES (?,?);''';
      List<dynamic> params = [breed.id, breed.name];
      await db.rawInsert(sql, params);
    } catch (exception) {}
  }
}
