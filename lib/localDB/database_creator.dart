import 'dart:io';

import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';

import 'BreedDAO.dart';
import 'DogDAO.dart';
import 'OwnerDAO.dart';
import 'WalkDAO.dart';
import 'WalkerDAO.dart';

Database db;

class DatabaseCreator {
  Future<String> getDatabasePath(String dbName) async {
    final databasePath = await getDatabasesPath();
    final path = join(databasePath, dbName);

    //make sure the folder exists
    if (await Directory(dirname(path)).exists()) {
      //await deleteDatabase(path);
    } else {
      await Directory(dirname(path)).create(recursive: true);
    }
    return path;
  }

  Future<void> initDatabase() async {
    final path = await getDatabasePath('canem');
    db = await openDatabase(path, version: 1, onCreate: onCreate);
  }

  Future<void> onCreate(Database db, int version) async {
    await WalkDAO().createWalkTable(db);
    await WalkerDAO().createWalkerTable(db);
    await OwnerDAO().createOwnerTable(db);
    await DogDAO().createDogTable(db);
    await BreedDAO().createBreedTable(db);
  }
}
