import 'package:canem_app/models/walkerDTO.dart';
import 'package:sqflite/sqflite.dart';

import 'database_creator.dart';

class WalkerDAO {
  static const walkerTable = 'walkerTable';

  Future<void> createWalkerTable(Database db) async {
    final query = '''CREATE TABLE $walkerTable
    (
      id TEXT PRIMARY KEY,
      name TEXT,
      email TEXT,
      uid TEXT,
      identification TEXT,
      rating REAL,
      imageURL TEXT,
      status INTEGER
    );''';
    await db.execute(query);
  }

  static Future<void> deleteAllWalkerTable() async {
    final query = '''DELETE FROM $walkerTable;''';
    await db.execute(query);
  }

  static Future<List<Walker>> getAllWalkers() async {
    try {
      final sql = '''SELECT * FROM $walkerTable  ''';
      final data = await db.rawQuery(sql);
      List<Walker> walkers = [];
      for (final node in data) {
        final walker = Walker.fromJson(node);
        walkers.add(walker);
      }
      return walkers;
    } catch (exception) {
      return [];
    }
  }

  static Future<void> addWalker(Walker walker) async {
    try {
      final sql = '''INSERT INTO $walkerTable
    (
      id,
      name,
      email,
      uid,
      identification,
      rating,
      imageURL,
      status
    )
    VALUES (?,?,?,?,?,?,?,?);''';
      int status;
      walker.status == true ? status = 1 : status = 0;
      List<dynamic> params = [
        walker.id,
        walker.name,
        walker.email,
        walker.uid,
        walker.identification,
        walker.rating,
        walker.imageURL,
        status
      ];
      await db.rawInsert(sql, params);
    } catch (exception) {}
  }
}
