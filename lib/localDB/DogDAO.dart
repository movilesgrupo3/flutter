import 'package:canem_app/models/dogDTO.dart';
import 'package:sqflite/sqflite.dart';

import 'database_creator.dart';

class DogDAO {
  static const dogTable = 'dogTable';

  Future<void> createDogTable(Database db) async {
    final query = '''CREATE TABLE $dogTable
    (
      id TEXT PRIMARY KEY,
      name TEXT,
      age INTEGER,
      imageURL TEXT
    );''';
    await db.execute(query);
  }

  static Future<void> deleteAllDogTable() async {
    final query = '''DELETE FROM $dogTable;''';
    await db.execute(query);
  }

  static Future<List<Dog>> getAllDogs() async {
    try {
      final sql = '''SELECT * FROM $dogTable  ''';
      final data = await db.rawQuery(sql);
      List<Dog> dogs = [];
      for (final node in data) {
        final dog = Dog.fromJson(node);
        dogs.add(dog);
      }
      return dogs;
    } catch (exception) {
      return [];
    }
  }

  static Future<void> addDog(Dog dog) async {
    try {
      final sql = '''INSERT INTO $dogTable
    (
      id,
      name,
      age,
      imageURL
    )
    VALUES (?,?,?,?);''';
      List<dynamic> params = [dog.id, dog.name, dog.age, dog.imageURL];
      await db.rawInsert(sql, params);
    } catch (exception) {}
  }
}
