import 'package:canem_app/models/ownerDTO.dart';
import 'package:sqflite/sqflite.dart';

import 'database_creator.dart';

class OwnerDAO {
  static const ownerTable = 'ownerTable';

  Future<void> createOwnerTable(Database db) async {
    final query = '''CREATE TABLE $ownerTable
    (
      id TEXT PRIMARY KEY,
      name TEXT,
      email TEXT,
      uid TEXT,
      imageURL TEXT
    );''';
    await db.execute(query);
  }

  static Future<void> deleteAllOwnerTable() async {
    final query = '''DELETE FROM $ownerTable;''';
    await db.execute(query);
  }

  static Future<List<Owner>> getAllOwners() async {
    try {
      final sql = '''SELECT * FROM $ownerTable  ''';
      final data = await db.rawQuery(sql);
      List<Owner> owners = [];
      for (final node in data) {
        final owner = Owner.fromJson(node);
        owners.add(owner);
      }
      return owners;
    } catch (exception) {
      return [];
    }
  }

  static Future<void> addOwner(Owner owner) async {
    try {
      final sql = '''INSERT INTO $ownerTable
    (
      id,
      name,
      email,
      uid,
      imageURL
    )
    VALUES (?,?,?,?,?);''';
      List<dynamic> params = [
        owner.id,
        owner.name,
        owner.email,
        owner.uid,
        owner.imageURL
      ];
      await db.rawInsert(sql, params);
    } catch (exception) {}
  }
}
