import 'dart:convert';

import 'package:canem_app/localDB/BreedDAO.dart';
import 'package:canem_app/models/breedDTO.dart';
import 'package:canem_app/shared/shared.dart';
import 'package:flutter/cupertino.dart';
import 'package:http/http.dart' as http;

class BreedsProvider with ChangeNotifier {
  List<Breed> _breeds;

  BreedsProvider(this._breeds);

  List<Breed> get breeds {
    return [..._breeds];
  }

  Future<void> fetchBreeds() async {
    final url = Shared.backIP + "breeds";
    try {
      return http.get(url, headers: {
        'Content-Type': 'application/json',
        'Accept': 'application/json',
      }).then((response) async {
        final responseData = json.decode(response.body) as List<dynamic>;
        final List<Breed> loadedBreeds = responseData.map((breed) {
          return Breed(
            id: breed["id"],
            name: breed["name"],
          );
        }).toList();
        _breeds = loadedBreeds;
        int size = _breeds.length;
        await BreedDAO.deleteAllBreedTable();
        for (int i = 0; i < size; i++) {
          await BreedDAO.addBreed(_breeds[i]);
        }
        notifyListeners();
      });
    } catch (error) {
      throw error;
    }
  }
}
