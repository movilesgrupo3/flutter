import 'dart:convert';

import 'package:canem_app/localDB/WalkerDAO.dart';
import 'package:canem_app/shared/shared.dart';
import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:http/http.dart' as http;

import '../models/walkerDTO.dart';

class WalkerProvider with ChangeNotifier {
  List<Walker> _walkers;
  Walker _walker;
  final String authToken;

  WalkerProvider(this.authToken, this._walkers, this._walker);

  List<Walker> get walkers {
    return [..._walkers];
  }

  Walker get walker {
    return _walker;
  }

  Future<dynamic> fetchWalkersAvailable() async {
    return Geolocator.getCurrentPosition(desiredAccuracy: LocationAccuracy.high)
        .then((location) {
      var identification = Shared.parseCoordinatesToString(
          location.latitude, location.longitude);
      final url = Shared.backIP + "walkers/nearwalkers/" + identification;
      try {
        return http.get(url, headers: {
          'Content-Type': 'application/json',
          'Accept': 'application/json',
        }).then((response) {
          final responseData = json.decode(response.body) as List<dynamic>;
          final List<Walker> loadedWalkers = responseData.map((walker) {
            return Walker(
              id: walker["id"],
              name: walker["name"],
              email: walker["email"],
              identification: walker["identification"],
              rating: walker["rating"].toDouble(),
              imageURL: walker["imageURL"],
              status: walker["status"],
            );
          }).toList();
          _walkers = [];
          int size = loadedWalkers.length;
          for (int i = 0; i < size; i++) {
            if (loadedWalkers[i].status) {
              _walkers.add(loadedWalkers[i]);
            }
          }
          notifyListeners();
        });
      } catch (error) {
        throw error;
      }
    });
  }

  Future<void> fetchWalkerUID(String uid) async {
    final url = Shared.backIP + "walkers/uid/$uid";
    try {
      return http.get(url, headers: {
        'Content-Type': 'application/json',
        'Accept': 'application/json',
      }).then((response) {
        final responseData = json.decode(response.body) as dynamic;
        final Walker loadedWalker = new Walker(
          id: responseData["id"],
          name: responseData["name"],
          email: responseData["email"],
          uid: responseData["uid"],
          identification: responseData["identification"],
          rating: responseData["rating"].toDouble(),
          imageURL: responseData["imageURL"],
          status: responseData["status"],
        );
        _walker = loadedWalker;
        WalkerDAO.deleteAllWalkerTable().then((value) {
          WalkerDAO.addWalker(_walker);
          notifyListeners();
        });
      });
    } catch (error) {
      throw error;
    }
  }

  Future<void> fetchWalker(String id) async {
    final url = Shared.backIP + "walkers/$id";
    try {
      return http.get(url, headers: {
        'Content-Type': 'application/json',
        'Accept': 'application/json',
      }).then((response) async {
        final responseData = json.decode(response.body) as dynamic;
        final Walker loadedWalker = new Walker(
          id: responseData["id"],
          name: responseData["name"],
          email: responseData["email"],
          uid: responseData["uid"],
          identification: responseData["identification"],
          rating: responseData["rating"].toDouble(),
          imageURL: responseData["imageURL"],
          status: responseData["status"],
        );
        _walker = loadedWalker;
        await WalkerDAO.deleteAllWalkerTable();
        await WalkerDAO.addWalker(_walker);
        notifyListeners();
      });
    } catch (error) {
      throw error;
    }
  }

  Future<void> updateWalker(Walker walker) async {
    final url = Shared.backIP + "walkers/" + walker.id;
    try {
      return http
          .put(url,
              headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer $authToken',
              },
              body: jsonEncode({
                'id': walker.id,
                'name': walker.name,
                'email': walker.email,
                'uid': walker.uid,
                'identification': walker.identification,
                'rating': walker.rating,
                'imageURL': walker.imageURL,
                'status': walker.status,
              }))
          .then((value) => notifyListeners());
    } catch (error) {
      throw error;
    }
  }
}
