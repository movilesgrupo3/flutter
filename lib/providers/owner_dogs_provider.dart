import 'dart:convert';

import 'package:canem_app/localDB/DogDAO.dart';
import 'package:canem_app/models/dogDTO.dart';
import 'package:canem_app/shared/shared.dart';
import 'package:flutter/cupertino.dart';
import 'package:http/http.dart' as http;

class OwnerDogsProvider with ChangeNotifier {
  List<Dog> _dogs;
  final String authToken;

  OwnerDogsProvider(this.authToken, this._dogs);

  List<Dog> get dogs {
    return [..._dogs];
  }

  Future<void> fetchOwnerDogs() async {
    final url = Shared.backIP + "owners/ownerPets";
    try {
      return http.get(url, headers: {
        'Content-Type': 'application/json',
        'Accept': 'application/json',
        'Authorization': 'Bearer $authToken',
      }).then((response) async {
        final responseData = json.decode(response.body) as List<dynamic>;
        final List<Dog> loadedWalks = responseData.map((dog) {
          return Dog(
              id: dog["id"],
              name: dog["name"],
              age: dog["age"],
              imageURL: dog["imageURL"],
              ownerId: dog["ownerId"],
              breedId: dog["breedId"]);
        }).toList();
        _dogs = loadedWalks;
        int size = _dogs.length;
        await DogDAO.deleteAllDogTable();
        for (int i = 0; i < size; i++) {
          await DogDAO.addDog(_dogs[i]);
        }
        notifyListeners();
      });
    } catch (error) {
      throw error;
    }
  }

  Future<void> createOwnerDog(
      String name, String age, String breed, String idOwner) async {
    final url = Shared.backIP + "dogs";
    try {
      return http.post(url,
          headers: {
            'Content-Type': 'application/json',
            'Accept': 'application/json',
          },
          body: json.encode({
            "name": name,
            "age": age,
            "imageURL": "http://placeimg.com/640/480",
            "ownerId": idOwner,
            "breedId": breed,
          }));
    } catch (error) {
      throw error;
    }
  }

  Future<void> deleteDog(String id) async {
    final url = Shared.backIP + "dogs/$id";
    try {
      return http.delete(url, headers: {
        'Content-Type': 'application/json',
        'Accept': 'application/json',
      });
    } catch (error) {
      throw error;
    }
  }
}
