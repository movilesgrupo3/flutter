import 'dart:convert';

import 'package:canem_app/models/walkDTO.dart';
import 'package:canem_app/shared/shared.dart';
import 'package:flutter/cupertino.dart';
import 'package:http/http.dart' as http;

class WalkProvider with ChangeNotifier {
  Walk _walk;

  WalkProvider(this._walk);

  Walk get walk {
    return _walk;
  }

  Future<void> postWalk(String idWalker, String idOwner) async {
    final url = Shared.backIP + "walks/";
    try {
      return http
          .post(url,
              headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/json',
              },
              body: jsonEncode(<String, dynamic>{
                "price": 6.0,
                "distanceTraveled": 0.0,
                "stepsWalked": 0,
                "time": 0.0,
                "status": 0,
                "dogId": null,
                "ownerId": "$idOwner",
                "walkerId": "$idWalker",
                "neighborhoodGroupId": null,
              }))
          .then((value) => notifyListeners());
    } catch (error) {
      throw error;
    }
  }

  Future<void> updateWalk(Walk walkParam) async {
    final url = Shared.backIP + "walks/${walkParam.id}";
    try {
      return http
          .put(url,
              headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/json',
              },
              body: jsonEncode(<String, dynamic>{
                "distanceTraveled": "${walkParam.distanceTraveled}",
                "stepsWalked": "${walkParam.stepsWalked}",
                "time": "${walkParam.time}",
                "price": "${walkParam.price}",
                "status": "${walkParam.status}",
              }))
          .then((value) => notifyListeners());
    } catch (error) {
      throw error;
    }
  }

  Future<Walk> fetchWalk(String id) async {
    final url = Shared.backIP + "walks/$id";
    try {
      return http.get(url, headers: {
        'Content-Type': 'application/json',
        'Accept': 'application/json',
      }).then((response) {
        notifyListeners();
        final walk = json.decode(response.body) as dynamic;
        return Walk(
          id: walk["id"],
          date: DateTime.parse(walk["date"]),
          price: double.parse(walk["price"].toString()),
          distanceTraveled: double.parse(walk["distanceTraveled"].toString()),
          stepsWalked: int.parse(walk["stepsWalked"].toString()),
          time: double.parse(walk["time"].toString()),
          status: int.parse(walk["status"].toString()),
          dogId: walk["dogId"],
          ownerId: walk["ownerId"],
          walkerId: walk["walkerId"],
          neighborhoodGroupId: walk["neighborhoodGroupId"],
        );
      });
    } catch (error) {
      throw error;
    }
  }
}
