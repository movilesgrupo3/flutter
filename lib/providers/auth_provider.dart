import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:canem_app/shared/shared.dart';
import 'package:flutter/material.dart';
import "package:http/http.dart" as http;
import 'package:flutter_secure_storage/flutter_secure_storage.dart';

import '../models/http_exception.dart';

class Auth with ChangeNotifier {
  String _token;
  DateTime _expiryDate;
  String _userId;
  String _role;
  Timer _authTimer;
  static final FlutterSecureStorage secureStorage = FlutterSecureStorage();

  static Future setInfo(String info) async => await secureStorage.write(key: "userData", value: info);
  static Future<String> getInfo() async => await secureStorage.read(key: "userData");
  static Future<void> deleteInfo() async => await secureStorage.delete(key: "userData");

  bool get isAuthenticated {
    return token != null;
  }

  String get token {
    if (_token != null &&
        _expiryDate != null &&
        _expiryDate.isAfter(DateTime.now())) {
      return _token;
    }
    return null;
  }

  String get role {
    if (isAuthenticated) {
      return _role;
    }
    return null;
  }

  String get userId {
    if (isAuthenticated) {
      return _userId;
    }
    return null;
  }

  Future<void> _authenticate(
    String name,
    String email,
    String password,
    String role,
    String fragment,
  ) async {
    const API_KEY = "AIzaSyC2DkzSqcg66wbq3T_Pv3Od8jhwiFMmzPY";
    final firebaseAuthUrl =
        "https://identitytoolkit.googleapis.com/v1/accounts:$fragment?key=$API_KEY";

    try {
      final response = await http.post(firebaseAuthUrl,
          body: json.encode({
            "email": email,
            "password": password,
            "returnSecureToken": true,
          }));

      final responseData = json.decode(response.body);

      if (responseData["error"] != null) {
        throw HttpException(responseData["error"]["message"]);
      } else {
        _token = responseData["idToken"];
        _userId = responseData["localId"];
        _expiryDate = DateTime.now()
            .add(Duration(seconds: int.parse(responseData["expiresIn"])));

        if (fragment == "signUp") {
          try {
            var backendFragment = role == "Owner" ? "owners" : "walkers";
            final backendUrl = Shared.backIP + "$backendFragment";

            await http.post(backendUrl,
                headers: {
                  HttpHeaders.contentTypeHeader: 'application/json',
                },
                body: json.encode({
                  "name": name,
                  "email": email,
                  "uid": _userId,
                }));

            final firebaseRolesUrl =
                "https://canem-app-default-rtdb.firebaseio.com/userRoles/$userId.json";
            await http.put(
              firebaseRolesUrl,
              body: json.encode({
                "role": role,
              }),
            );
            _role = role;
          } catch (error) {}
        } else {
          final firebaseRolesUrl =
              "https://canem-app-default-rtdb.firebaseio.com/userRoles/$userId.json";
          final rolesResponse = await http.get(firebaseRolesUrl);
          final rolesResponseData =
              json.decode(rolesResponse.body) as Map<String, dynamic>;
          _role = rolesResponseData["role"];
        }

        _autoLogOut();

        notifyListeners();

        final userData = json.encode({
          "token": _token,
          "userId": _userId,
          "expiryDate": _expiryDate.toIso8601String(),
          "role": _role,
        });
        await setInfo(userData);
      }
    } catch (error) {
      throw error;
    }
  }

  Future<void> signUp(
      String name, String email, String password, String role) async {
    return _authenticate(name, email, password, role, "signUp");
  }

  Future<void> signIn(String email, String password) async {
    return _authenticate(null, email, password, null, "signInWithPassword");
  }

  Future<bool> tryAutoLogin() async {
    var data = await getInfo();
    if (data==null) {
      return false;
    }
    final userData = json.decode(data) as Map<String, Object>;

    final expiryDate = DateTime.parse(userData["expiryDate"]);

    if (expiryDate.isBefore(DateTime.now())) {
      return false;
    }

    _token = userData["token"];
    _userId = userData["userId"];
    _role = userData["role"];
    _expiryDate = expiryDate;
    notifyListeners();
    _autoLogOut();
    return true;
  }

  Future<void> logOut() async {
    _token = null;
    _userId = null;
    _expiryDate = null;
    if (_authTimer != null) {
      _authTimer.cancel();
      _authTimer = null;
    }
    notifyListeners();
    await deleteInfo();
  }

  void _autoLogOut() {
    if (_authTimer != null) {
      _authTimer.cancel();
    }

    final timeToExpiry = _expiryDate.difference(DateTime.now()).inSeconds;
    _authTimer = Timer(Duration(seconds: timeToExpiry), logOut);
  }
}
