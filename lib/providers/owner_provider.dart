import 'dart:convert';

import 'package:canem_app/localDB/OwnerDAO.dart';
import 'package:canem_app/shared/shared.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

import '../models/ownerDTO.dart';

class OwnerProvider with ChangeNotifier {
  Owner _owner;

  OwnerProvider(this._owner);

  Owner get owner {
    return _owner;
  }

  Future<void> fetchOwnerUID(String uid) async {
    final url = Shared.backIP + "owners/uid/$uid";
    try {
      return http.get(url, headers: {
        'Content-Type': 'application/json',
        'Accept': 'application/json',
      }).then((response) async {
        final responseData = json.decode(response.body) as dynamic;
        final Owner loadedOwner = new Owner(
            id: responseData["id"],
            name: responseData["name"],
            email: responseData["email"],
            uid: responseData["uid"],
            imageURL: responseData["imageURL"],
            neighborhoodGroupId: responseData["neighborhoodGroupId"]);
        _owner = loadedOwner;
        await OwnerDAO.deleteAllOwnerTable();
        await OwnerDAO.addOwner(_owner);
        notifyListeners();
      });
    } catch (error) {
      throw error;
    }
  }
}
