import 'dart:convert';

import 'package:canem_app/localDB/WalkDAO.dart';
import 'package:canem_app/shared/shared.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

import '../models/walkDTO.dart';

class WalkerWalksProvider with ChangeNotifier {
  List<Walk> _walks;
  Walk _currentWalk;
  final String authToken;

  WalkerWalksProvider(this.authToken, this._walks);

  List<Walk> get walks {
    return [..._walks];
  }

  Walk get currentWalk {
    return _currentWalk;
  }

  Future<void> fetchWalksStatus(int status) async {
    final url = Shared.backIP + "walks/statusWalkerWalks/" + status.toString();
    try {
      return http.get(url, headers: {
        'Content-Type': 'application/json',
        'Accept': 'application/json',
        'Authorization': 'Bearer $authToken',
      }).then((response) {
        final responseData = json.decode(response.body) as List<dynamic>;
        final List<Walk> loadedWalks = responseData.map((walk) {
          return Walk(
            id: walk["id"],
            date: DateTime.parse(walk["date"]),
            price: double.parse(walk["price"].toString()),
            distanceTraveled: double.parse(walk["distanceTraveled"].toString()),
            stepsWalked: int.parse(walk["stepsWalked"].toString()),
            time: double.parse(walk["time"].toString()),
            status: int.parse(walk["status"].toString()),
            dogId: walk["dogId"],
            ownerId: walk["ownerId"],
            walkerId: walk["walkerId"],
            neighborhoodGroupId: walk["neighborhoodGroupId"],
          );
        }).toList();
        if(loadedWalks.isEmpty){
          _currentWalk = null;
        }
        else{
          _currentWalk = loadedWalks[0];
        }
      });
    } catch (error) {
      throw error;
    }
  }

  Future<void> fetchWalks() async {
    final url = Shared.backIP + "walks/statusWalkerWalks/2";
    try {
      return http.get(url, headers: {
        'Content-Type': 'application/json',
        'Accept': 'application/json',
        'Authorization': 'Bearer $authToken',
      }).then((response) async {
        final responseData = json.decode(response.body) as List<dynamic>;
        final List<Walk> loadedWalks = responseData.map((walk) {
          return Walk(
            id: walk["id"],
            date: DateTime.parse(walk["date"]),
            price: double.parse(walk["price"].toString()),
            distanceTraveled: double.parse(walk["distanceTraveled"].toString()),
            stepsWalked: int.parse(walk["stepsWalked"].toString()),
            time: double.parse(walk["time"].toString()),
            status: int.parse(walk["status"].toString()),
            dogId: walk["dogId"],
            ownerId: walk["ownerId"],
            walkerId: walk["walkerId"],
            neighborhoodGroupId: walk["neighborhoodGroupId"],
          );
        }).toList();
        _walks = loadedWalks;
        int size = _walks.length;
        _walks.sort((a, b) => b.date.compareTo(a.date));
        await WalkDAO.deleteAllWalkTable();
        for (int i = 0; i < size; i++) {
          await WalkDAO.addWalk(_walks[i]);
        }
        notifyListeners();
      });
    } catch (error) {
      throw error;
    }
  }
}
