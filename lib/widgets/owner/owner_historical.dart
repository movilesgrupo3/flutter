import 'package:canem_app/localDB/WalkDAO.dart';
import 'package:canem_app/providers/owner_walks_provider.dart';
import 'package:canem_app/shared/loading.dart';
import 'package:canem_app/utils/connectivity_utils.dart';
import 'package:canem_app/widgets/blueprints/walks_historical.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class OwnerHistorical extends StatefulWidget {
  @override
  _OwnerHistoricalState createState() => _OwnerHistoricalState();
}

class _OwnerHistoricalState extends State<OwnerHistorical> {
  Widget defaultWidget =
      Loading.showLoadingScreen("Your walk history is loading");
  bool widgetLoaded = false;

  @override
  void initState() {
    widgetLoaded = false;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    if (!widgetLoaded) {
      ConnectivityUtils.executeFunctionIfInternet(() {
        final ownerWalksProvider =
            Provider.of<OwnerWalksProvider>(context, listen: false);
        ownerWalksProvider.fetchWalks().then((value) {
          setState(() {
            widgetLoaded = true;
            defaultWidget =
                WalksHistorical().buildList(ownerWalksProvider.walks);
          });
        });
      }, () {
        WalkDAO.getAllWalks().then((value) {
          setState(() {
            widgetLoaded = true;
            defaultWidget = WalksHistorical().buildList(value);
          });
        });
      });
    }
    return defaultWidget;
  }
}
