import 'package:canem_app/shared/loading.dart';
import 'package:canem_app/shared/session_state.dart';
import 'package:canem_app/widgets/owner/searchWalkSteps/available_walkers.dart';
import 'package:canem_app/widgets/owner/searchWalkSteps/base.dart';
import 'package:canem_app/widgets/owner/searchWalkSteps/live_walk.dart';
import 'package:canem_app/widgets/owner/searchWalkSteps/waiting.dart';
import 'package:flutter/material.dart';

class OwnerSearchWalk extends StatefulWidget {
  @override
  OwnerSearchWalkState createState() => OwnerSearchWalkState();
}

class OwnerSearchWalkState extends State<OwnerSearchWalk> {
  static const BASE_WIDGET = 0;
  static const AVAIlABLE_LIST = 1;
  static const WAITING_FOR_WALKER = 2;
  static const LIVE_WALK = 3;

  Widget defaultWidget =
      Loading.showLoadingScreen("Canem is searching for nearby walkers");
  Base base;
  AvailableWalkers availableWalkers;
  Waiting waiting;
  LiveWalk liveWalk;

  @override
  void dispose() {
    if(SessionState.walkState==AVAIlABLE_LIST){
      SessionState.walkState=BASE_WIDGET;
    }
      if ((SessionState.walkState == BASE_WIDGET || SessionState.walkState == AVAIlABLE_LIST)&& Base.timer != null) {
        Base.timer.cancel();
        Base.timer = null;
      }
    if (liveWalk != null) {
      if (SessionState.walkState == LIVE_WALK && liveWalk.timer != null) {
        liveWalk.timer.cancel();
        liveWalk.timer = null;
      }
    }
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    switch (SessionState.walkState) {
      case BASE_WIDGET:
        if (base == null) {
          base = Base();
        }
        base.baseWidget(this);
        liveWalk = null;
        break;
      case AVAIlABLE_LIST:
        if (availableWalkers == null) {
          availableWalkers = AvailableWalkers();
        }
        availableWalkers.buildSearch(this);
        base = null;
        break;
      case WAITING_FOR_WALKER:
        if (waiting == null) {
          waiting = Waiting();
        }
        waiting.waitingForWalker(this);
        availableWalkers = null;
        break;
      case LIVE_WALK:
        if (liveWalk == null) {
          liveWalk = LiveWalk();
        }
        liveWalk.currentWalk(this);
        waiting = null;
        break;
    }
    return defaultWidget;
  }
}
