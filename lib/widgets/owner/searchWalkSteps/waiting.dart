import 'package:canem_app/shared/session_state.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import '../owner_search_walk.dart';

class Waiting {
  void waitingForWalker(OwnerSearchWalkState state) {
    state.setState(() {
      state.defaultWidget = Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Text("Waiting for the walker's response",
              style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
              textAlign: TextAlign.center,
              overflow: TextOverflow.ellipsis),
          Container(
              height: 400,
              width: 400,
              alignment: Alignment.center,
              child: CircularProgressIndicator(
                valueColor: AlwaysStoppedAnimation<Color>(Colors.deepPurple),
              )),
          ElevatedButton(
              onPressed: () {
                state.setState(() {
                  SessionState.walkState = OwnerSearchWalkState.BASE_WIDGET;
                });
              },
              child: Text(
                "Cancel search",
                style: TextStyle(fontSize: 25),
              ),
              style: ElevatedButton.styleFrom(padding: EdgeInsets.all(15))),
        ],
      );
    });
  }
}
