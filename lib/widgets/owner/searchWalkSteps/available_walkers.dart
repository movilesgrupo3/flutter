import 'dart:async';

import 'package:canem_app/models/walkerDTO.dart';
import 'package:canem_app/providers/owner_walks_provider.dart';
import 'package:canem_app/providers/walk_provider.dart';
import 'package:canem_app/shared/session_state.dart';
import 'package:canem_app/shared/shared.dart';
import 'package:canem_app/utils/connectivity_utils.dart';
import 'package:canem_app/widgets/owner/searchWalkSteps/base.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:provider/provider.dart';

import '../owner_search_walk.dart';

class AvailableWalkers {
  Timer timer;

  void buildSearch(OwnerSearchWalkState state) {
    state.setState(() {
      state.defaultWidget = Scaffold(
          appBar: AppBar(
              backgroundColor: Colors.white,
              title: Text(
                "Available Walkers",
                style: TextStyle(color: Colors.deepPurple),
              )),
          body: buildElements(state));
    });
  }

  Widget buildElements(OwnerSearchWalkState state) {
    if (SessionState.fetchedWalkers.isEmpty) {
      return Scaffold(
          body: Center(
              child: Text(
                  'It looks like there are no walkers available at the moment')));
    }
    return Container(
        child: ListView.builder(
            itemCount: SessionState.fetchedWalkers.length,
            scrollDirection: Axis.vertical,
            itemBuilder: (BuildContext context, int index) {
              var theWalker = SessionState.fetchedWalkers[index];
              var coords = Shared.parseCoordinates(theWalker.identification);
              var markersWalker = new Set<Marker>();
              markersWalker.add(Marker(
                  markerId: MarkerId(theWalker.id),
                  position: LatLng(coords[0], coords[1])));
              return Column(children: [
                Container(
                  child: SizedBox(
                      width: 500,
                      height: 250,
                      child: Padding(
                          padding: const EdgeInsets.all(20),
                          child: GoogleMap(
                            markers: markersWalker,
                            initialCameraPosition: CameraPosition(
                                target: LatLng(coords[0], coords[1]), zoom: 12),
                          ))),
                ),
                createwalkertile(state, theWalker),
              ]);
            }));
  }

  Widget createwalkertile(OwnerSearchWalkState state, Walker walker) {
    return Column(
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            Icon(
              Icons.account_box_outlined,
              size: 35,
            ),
            Text(
              "${walker.name}",
              style: TextStyle(fontSize: 25, fontWeight: FontWeight.bold),
            ),
            InkWell(
              onTap: () {
                ConnectivityUtils.executeFunctionIfInternet(() {
                  selectWalker(walker, state);
                }, () {
                  ConnectivityUtils.showConnectivityAlert(
                      ConnectivityUtils.defaultTitle,
                      ConnectivityUtils.defaultContent,
                      state.context);
                });
              },
              child: Container(
                width: 70.0,
                height: 40.0,
                margin: EdgeInsets.all(10.0),
                decoration: BoxDecoration(
                    color: Colors.deepPurple,
                    borderRadius: BorderRadius.circular(7.0)),
                child: Center(
                  child: Text(
                    "Hire",
                    style: TextStyle(
                        color: Colors.white,
                        fontSize: 17.0,
                        fontWeight: FontWeight.bold),
                  ),
                ),
              ),
            ),
          ],
        ),
        Divider(
          color: Colors.deepPurple,
          thickness: 1.25,
          indent: 15,
        )
      ],
    );
  }

  void selectWalker(Walker walker, OwnerSearchWalkState state) {
    Base.timer.cancel();
    Base.timer = null;

    state.setState(() {
      SessionState.walkState = OwnerSearchWalkState.WAITING_FOR_WALKER;
    });
    Provider.of<WalkProvider>(state.context)
        .postWalk(walker.id, SessionState.user.id);
    SessionState.currentWalker = walker;
    timer = Timer.periodic(Duration(seconds: 1), (timer) {
      ConnectivityUtils.executeFunctionIfInternet(() {
        final ownerWalksProvider =
            Provider.of<OwnerWalksProvider>(state.context, listen: false);
        ownerWalksProvider.fetchWalksStatus(1).then((value) {
          SessionState.currentWalk = ownerWalksProvider.currentWalk;
          if (SessionState.currentWalk != null) {
            state.setState(() {
              SessionState.walkState = OwnerSearchWalkState.LIVE_WALK;
            });
            this.timer.cancel();
            this.timer = null;
          }
        });
      }, () {
        state.setState(() {
          state.defaultWidget = Center(
              child: Text(
            "Your device must be connected to Internet in order to access this functionality",
            textAlign: TextAlign.center,
          ));
        });
      });
    });
  }
}
