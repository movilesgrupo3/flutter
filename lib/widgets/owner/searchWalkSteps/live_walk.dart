import 'dart:async';

import 'package:canem_app/providers/walk_provider.dart';
import 'package:canem_app/providers/walker_provider.dart';
import 'package:canem_app/shared/session_state.dart';
import 'package:canem_app/shared/shared.dart';
import 'package:canem_app/utils/connectivity_utils.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:provider/provider.dart';
import 'package:rating_dialog/rating_dialog.dart';

import '../owner_search_walk.dart';

class LiveWalk {
  Set<Marker> markers;
  Timer timer;

  Widget currentWalk(OwnerSearchWalkState state) {
    var coords =
        Shared.parseCoordinates(SessionState.currentWalker.identification);
    var lat = coords[0];
    var long = coords[1];
    state.setState(() {
      state.defaultWidget = SingleChildScrollView(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Padding(
                  padding: const EdgeInsets.only(top: 20),
                  child: Text("Your walk is on progress",
                      style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold))),
              SizedBox(
                  width: MediaQuery.of(state.context).size.width,
                  height: 400,
                  child: Padding(
                      padding: const EdgeInsets.all(20),
                      child: GoogleMap(
                        onMapCreated: (GoogleMapController controller) {
                          onMapCreated(controller, state);
                        },
                        markers: markers,
                        initialCameraPosition:
                        CameraPosition(target: LatLng(lat, long), zoom: 12),
                      ))),
            ],
          ));
    });

  }

  void onMapCreated(
      GoogleMapController controller, OwnerSearchWalkState state) {
    updateMap(controller, state);
    if (timer == null) {
      timer = Timer.periodic(Duration(seconds: 1), (timer) {
        ConnectivityUtils.executeFunctionIfInternet(() {
          final walkerProvider =
              Provider.of<WalkerProvider>(state.context, listen: false);
          walkerProvider
              .fetchWalker(SessionState.currentWalker.id)
              .then((value) {
            SessionState.currentWalker.identification =
                walkerProvider.walker.identification;
            updateMap(controller, state);
            var walkProvider =
                Provider.of<WalkProvider>(state.context, listen: false);
            walkProvider.fetchWalk(SessionState.currentWalk.id).then((value) {
              if (value.status == 2) {
                state.setState(() {
                  var tempWalk = SessionState.currentWalk;
                  showDialog(
                      context: state.context,
                      builder: (context) {
                        return RatingDialog(
                          icon: Image.asset('assets/images/canem_logo.png'),
                          title: "Rate the walk",
                          description: "Tap a star to set your rating",
                          onSubmitPressed: (int raiting) {
                            tempWalk.price = raiting.toDouble();
                            tempWalk.status = 2;
                            Provider.of<WalkProvider>(context, listen: false)
                                .updateWalk(tempWalk);
                          },
                          submitButton: "Send review",
                        );
                      });
                  SessionState.walkState = OwnerSearchWalkState.BASE_WIDGET;
                  SessionState.currentWalk = null;
                  SessionState.currentWalker = null;
                  this.timer.cancel();
                  this.timer=null;
                });
              }
            });
          });
        }, () {
          state.setState(() {
            state.defaultWidget = Center(
                child: Text(
              "Your device must be connected to Internet in order to access this functionality",
              textAlign: TextAlign.center,
            ));
          });
        });
      });
    }
    ;
  }

  void updateMap(GoogleMapController controller, OwnerSearchWalkState state) {
    state.setState(() {
      var coords =
          Shared.parseCoordinates(SessionState.currentWalker.identification);
      var lat = coords[0];
      var long = coords[1];
      markers = new Set<Marker>();
      markers.add(Marker(
          markerId: MarkerId(SessionState.currentWalker.id),
          position: LatLng(lat, long)));
      controller.moveCamera(CameraUpdate.newCameraPosition(
          CameraPosition(target: LatLng(lat, long), zoom: 12)));
    });
  }
}
