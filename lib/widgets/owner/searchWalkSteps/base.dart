import 'dart:async';

import 'package:canem_app/providers/owner_walks_provider.dart';
import 'package:canem_app/providers/walker_provider.dart';
import 'package:canem_app/shared/session_state.dart';
import 'package:canem_app/utils/connectivity_utils.dart';
import 'package:canem_app/utils/gps_utils.dart';
import 'package:canem_app/widgets/owner/owner_search_walk.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:provider/provider.dart';

class Base {
  static Timer timer;
  void baseWidget(OwnerSearchWalkState state) {
    ConnectivityUtils.executeFunctionIfInternet(() {
      final ownerWalksData =
          Provider.of<OwnerWalksProvider>(state.context, listen: false);
      if (ownerWalksData.shouldRememberWalk) {
        SchedulerBinding.instance.addPostFrameCallback((_) {
          _showErrorDialog(state);
        });
      }
      if (Base.timer == null) {
        final walkerProvider = Provider.of<WalkerProvider>(state.context);
        Base.timer = Timer.periodic(Duration(seconds: 1), (timer) {
          ConnectivityUtils.executeFunctionIfInternet(() {
            GPSUtils.executeFunctionIfGPS(() {
              walkerProvider.fetchWalkersAvailable().then((value) {
                if(SessionState.currentWalk==null){
                  state.setState(() {
                    SessionState.fetchedWalkers = walkerProvider.walkers;
                    SessionState.walkState = OwnerSearchWalkState.AVAIlABLE_LIST;
                  });
                }
                else{
                  Base.timer.cancel();
                  Base.timer=null;
                }
              });
            }, () {
              state.setState(() {
                state.defaultWidget = Center(
                    child: Text(
                  "Your device needs location permissions and location activated in order to search for walkers near your location",
                  textAlign: TextAlign.center,
                ));
              });
            });
          }, () {
            state.setState(() {
              state.defaultWidget = Center(
                  child: Text(
                "Your device must be connected to Internet in order to access this functionality",
                textAlign: TextAlign.center,
              ));
            });
          });
        });
      }
    }, () {
      state.setState(() {
        state.defaultWidget = Center(
            child: Text(
          "Your device must be connected to Internet in order to access this functionality",
          textAlign: TextAlign.center,
        ));
      });
    });
  }

  void _showErrorDialog(State state) {
    showDialog(
      context: state.context,
      builder: (ctx) => AlertDialog(
        title: Text("Do you want to ask for a walk for your pets?"),
        content: Text(
            "You haven't asked your dogs for a walk for more than 3 days. You should consider doing it soon, as this could benefit their health."),
        actions: [
          FlatButton(
            onPressed: () => Navigator.of(state.context).pop(),
            child: Text("I am not interested."),
          ),
          FlatButton(
            onPressed: () => Navigator.of(state.context).pop(),
            child: Text("Sure!"),
          ),
        ],
      ),
    );
  }
}
