import 'package:canem_app/shared/loading.dart';
import 'package:canem_app/shared/session_state.dart';
import 'package:canem_app/utils/connectivity_utils.dart';
import 'package:canem_app/widgets/blueprints/profile.dart';
import 'package:flutter/material.dart';

class OwnerProfile extends StatefulWidget {
  @override
  _OwnerProfileState createState() => _OwnerProfileState();
}

class _OwnerProfileState extends State<OwnerProfile> {
  Widget defaultWidget = Loading.showLoadingScreen("Your profile is loading");
  bool widgetLoaded = false;

  @override
  Widget build(BuildContext context) {
    if (!widgetLoaded) {
      ConnectivityUtils.executeFunctionIfInternet(() {
        setState(() {
          widgetLoaded = true;
          defaultWidget =
              Profile().buildProfile(context, SessionState.user, "Owner");
        });
      }, () {
        if (!widgetLoaded) {
          setState(() {
            defaultWidget =
                Profile().buildProfile(context, SessionState.user, "Owner");
          });
        }
      });
    }
    return defaultWidget;
  }
}
