import 'package:canem_app/localDB/BreedDAO.dart';
import 'package:canem_app/localDB/DogDAO.dart';
import 'package:canem_app/providers/breed_provider.dart';
import 'package:canem_app/providers/owner_dogs_provider.dart';
import 'package:canem_app/shared/loading.dart';
import 'package:canem_app/utils/connectivity_utils.dart';
import 'package:canem_app/widgets/blueprints/dogs_registers.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class OwnerPets extends StatefulWidget {
  @override
  _OwnerPetsState createState() => _OwnerPetsState();
}

class _OwnerPetsState extends State<OwnerPets> {
  Widget defaultWidget =
      Loading.showLoadingScreen("Your dogs registers are loading");
  bool widgetLoaded = false;
  Function updateList;

  @override
  Widget build(BuildContext context) {
    if (!widgetLoaded) {
      updateList = () {
        setState(() {
          widgetLoaded = true;
        });
      };
      ConnectivityUtils.executeFunctionIfInternet(() {
        final ownerDogsProvider =
            Provider.of<OwnerDogsProvider>(context, listen: false);
        final ownerBreedsProvider =
            Provider.of<BreedsProvider>(context, listen: false);
        ownerDogsProvider.fetchOwnerDogs().then((value) {
          ownerBreedsProvider.fetchBreeds().then((res) {
            setState(() {
              widgetLoaded = false;
              defaultWidget = DogsRegisters().buildList(ownerDogsProvider.dogs,
                  ownerBreedsProvider.breeds, context, updateList);
            });
          });
        });
      }, () {
        DogDAO.getAllDogs().then((value) {
          BreedDAO.getAllBreeds().then((res) {
            setState(() {
              widgetLoaded = false;
              defaultWidget =
                  DogsRegisters().buildList(value, res, context, updateList);
            });
          });
        });
      });
    }
    return defaultWidget;
  }
}
