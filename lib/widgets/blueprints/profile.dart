import 'package:canem_app/providers/auth_provider.dart';
import 'package:canem_app/shared/session_state.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class Profile {
  Widget buildProfile(BuildContext context, dynamic user, String typeOfUser) {
    return ListView(
        scrollDirection: Axis.vertical,
        padding: EdgeInsets.all(10.5),
        children: [
          Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    Icon(
                      Icons.account_circle,
                      size: 100.0,
                    ),
                    Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          "${user.name}",
                          style: TextStyle(
                            fontWeight: FontWeight.bold,
                            fontSize: 35.0,
                          ),
                        ),
                        Text(typeOfUser)
                      ],
                    ),
                  ],
                ),
                Divider(
                  color: Colors.deepPurple,
                  height: 30.0,
                ),
              ],
            ),
          ),
          Container(
              padding: EdgeInsets.only(left: 10.0, right: 10.0, top: 10.0),
              height: MediaQuery.of(context).size.height * 0.35,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  Row(mainAxisAlignment: MainAxisAlignment.start, children: [
                    Icon(Icons.email),
                    Text(
                      "${user.email}",
                    ),
                  ]),
                  Row(mainAxisAlignment: MainAxisAlignment.start, children: [
                    Icon(Icons.verified),
                    Text("Account id: "),
                    Container(
                      child: Text(
                        "${user.id.substring(0, 20)}...",
                      ),
                    ),
                  ]),
                  Align(
                    alignment: Alignment.bottomCenter,
                    child: ElevatedButton(
                        onPressed: () {
                            Provider.of<Auth>(context, listen: false).logOut();
                            SessionState.logout();},
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [Icon(Icons.logout), Text("Logout")],
                        )),
                  ),
                ],
              ))
        ]);
  }
}
