import 'package:canem_app/models/walkDTO.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class WalksHistorical {
  Widget buildList(List<Walk> listWalks) {
    return Scaffold(
      appBar: AppBar(
          backgroundColor: Colors.white,
          title: Text(
            "History",
            style: TextStyle(color: Colors.deepPurple),
          )),
      body: Container(
          child: buildElements(listWalks))
    );
  }

  Widget buildElements(List<Walk> listWalks) {
    if (listWalks.isEmpty) {
      return Scaffold(
          body: Center(
              child: Text(
                  'It looks like you do not have any walk')));
    }
    return ListView.builder(
        itemCount: listWalks.length,
        itemBuilder: (BuildContext context, int index) {
          return Stack(children: [
            createWalkTile(context, listWalks[index]),
          ]);
        });
  }

  Widget createWalkTile(BuildContext context, Walk walk) {
    return Column(
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            Icon(
              Icons.directions_walk,
              size: 35,
            ),
            Text(
              DateFormat.yMd().add_jm().format(walk.date).toString(),
              style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold),
            ),
            Row(
              children: [
                Icon(Icons.star, color: Colors.yellow),
                Text(walk.price <= 5.0 ? walk.price.toString() : "N/A",
                    style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold))
              ],
            )
          ],
        ),
        Divider(
          color: Colors.deepPurple,
          thickness: 1.25,
          indent: 15,
        )
      ],
    );
  }
}
