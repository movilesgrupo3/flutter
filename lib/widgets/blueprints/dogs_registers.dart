import 'package:canem_app/models/breedDTO.dart';
import 'package:canem_app/models/dogDTO.dart';
import 'package:canem_app/providers/owner_dogs_provider.dart';
import 'package:canem_app/shared/session_state.dart';
import 'package:canem_app/utils/connectivity_utils.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:provider/provider.dart';

class DogsRegisters {
  Widget buildList(List<Dog> listDogs, List<Breed> listBreeds,
      BuildContext context, Function updateList) {
    final nameController = TextEditingController();
    final ageController = TextEditingController();
    Breed dropdownValue = listBreeds[0];
    return Scaffold(
      appBar: AppBar(
          backgroundColor: Colors.white,
          title: Text(
            "Owner Dogs",
            style: TextStyle(color: Colors.deepPurple),
          )),
      body: Container(child: buildElements(listDogs, updateList)),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          ConnectivityUtils.executeFunctionIfInternet(() {
            showDialog(
                context: context,
                builder: (context) {
                  return AlertDialog(
                    title: Text("Create a Dog"),
                    content: Container(
                      height: MediaQuery.of(context).size.height * 0.35,
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [
                          TextField(
                            controller: nameController,
                            decoration: InputDecoration(
                              border: OutlineInputBorder(),
                              labelText: 'Name',
                            ),
                          ),
                          TextField(
                            controller: ageController,
                            decoration: InputDecoration(
                              border: OutlineInputBorder(),
                              labelText: 'Age',
                            ),
                          ),
                          DropdownButton<Breed>(
                            value: dropdownValue,
                            icon: const Icon(Icons.arrow_downward),
                            iconSize: 24,
                            elevation: 16,
                            style: const TextStyle(color: Colors.deepPurple),
                            underline: Container(
                              height: 2,
                              color: Colors.deepPurpleAccent,
                            ),
                            onChanged: (value) {
                              dropdownValue = value;
                            },
                            items: listBreeds
                                .map<DropdownMenuItem<Breed>>((Breed value) {
                              return DropdownMenuItem<Breed>(
                                value: value,
                                child: Text(value.name),
                              );
                            }).toList(),
                          ),
                          ElevatedButton(
                            child: Text('Create'),
                            onPressed: () {
                              {
                                ConnectivityUtils.executeFunctionIfInternet(() {
                                  Fluttertoast.cancel();
                                  Fluttertoast.showToast(
                                      msg: "Please be patient, we are processing your request",
                                      toastLength: Toast.LENGTH_LONG,
                                      gravity: ToastGravity.CENTER
                                  );
                                  Provider.of<OwnerDogsProvider>(context,
                                          listen: false)
                                      .createOwnerDog(
                                          nameController.value.text,
                                          ageController.value.text,
                                          dropdownValue.id,
                                          SessionState.user.id)
                                      .then((value) {
                                    updateList();
                                  });
                                }, () {
                                  ConnectivityUtils.showConnectivityAlert(
                                      "An error ocurred in the Create a Dog process",
                                      "The device has lost internet connection which is required for this functionality",
                                      context);
                                });
                              }
                              Navigator.pop(context);
                            },
                          )
                        ],
                      ),
                    ),
                  );
                });
          }, () {
            ConnectivityUtils.showConnectivityAlert(
                "An error ocurred in the Create a Dog process",
                "The device has lost internet connection which is required for this functionality",
                context);
          });
        },
        child: const Icon(Icons.add),
        backgroundColor: Colors.green,
      ),
    );
  }

  Widget buildElements(List<Dog> listDogs, Function updateList) {
    if (listDogs.isEmpty) {
      return Scaffold(
          body:
              Center(child: Text('It looks like you have not registered any of your dogs')));
    }
    return ListView.builder(
        itemCount: listDogs.length,
        itemBuilder: (BuildContext context, int index) {
          return Stack(children: [
            createDogTile(context, listDogs[index], updateList),
          ]);
        });
  }

  Widget createDogTile(BuildContext context, Dog dog, Function updateList) {
    return Container(
      decoration: BoxDecoration(
        border: Border.all(color: Colors.deepPurple, style: BorderStyle.solid),
        borderRadius: BorderRadius.circular(12.0),
      ),
      margin: EdgeInsets.only(left: 10.0, right: 10.0, top: 10.0, bottom: 10.0),
      child: Column(
        children: [
          Container(
            decoration: BoxDecoration(
              border: Border.all(
                  color: Colors.deepPurple, style: BorderStyle.solid),
              color: Colors.deepPurple,
              borderRadius: BorderRadius.circular(12.0),
            ),
            padding: EdgeInsets.only(top: 10.0, bottom: 10.0),
            alignment: Alignment.center,
            child: Text(
              "${dog.name}",
              style: TextStyle(
                  fontSize: 20.0,
                  color: Colors.white,
                  fontWeight: FontWeight.bold),
            ),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              Icon(
                Icons.pets,
                size: 35,
              ),
              Text(
                "Age: ${dog.age}",
                style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold),
              ),
              ElevatedButton(
                  onPressed: () {
                    ConnectivityUtils.executeFunctionIfInternet(() {
                      Fluttertoast.cancel();
                      Fluttertoast.showToast(
                          msg: "Please be patient, we are processing your request",
                          toastLength: Toast.LENGTH_LONG,
                          gravity: ToastGravity.CENTER
                      );
                      Provider.of<OwnerDogsProvider>(context, listen: false)
                          .deleteDog(dog.id)
                          .then((value) {
                        updateList();
                      });
                    }, () {
                      ConnectivityUtils.showConnectivityAlert(
                          "An error ocurred in the Delete a Dog process",
                          "The device has lost internet connection which is required for this functionality",
                          context);
                    });
                  },
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [Text("Delete")],
                  )),
            ],
          ),
        ],
      ),
    );
  }
}
