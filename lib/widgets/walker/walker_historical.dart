import 'package:canem_app/localDB/WalkDAO.dart';
import 'package:canem_app/providers/walker_walks_provider.dart';
import 'package:canem_app/shared/loading.dart';
import 'package:canem_app/utils/connectivity_utils.dart';
import 'package:canem_app/widgets/blueprints/walks_historical.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class WalkerHistorical extends StatefulWidget {
  @override
  _WalkerHistoricalState createState() => _WalkerHistoricalState();
}

class _WalkerHistoricalState extends State<WalkerHistorical> {
  Widget defaultWidget =
      Loading.showLoadingScreen("Your walk history is loading");
  bool widgetLoaded = false;

  @override
  void initState() {
    widgetLoaded = false;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    if (!widgetLoaded) {
      ConnectivityUtils.executeFunctionIfInternet(() {
        final walkerWalksProvider =
            Provider.of<WalkerWalksProvider>(context, listen: false);
        walkerWalksProvider.fetchWalks().then((value) {
          setState(() {
            widgetLoaded = true;
            defaultWidget =
                WalksHistorical().buildList(walkerWalksProvider.walks);
          });
        });
      }, () {
        WalkDAO.getAllWalks().then((value) {
          setState(() {
            widgetLoaded = true;
            defaultWidget = WalksHistorical().buildList(value);
          });
        });
      });
    }
    return defaultWidget;
  }
}
