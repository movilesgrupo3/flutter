import 'package:canem_app/shared/loading.dart';
import 'package:canem_app/shared/session_state.dart';
import 'package:canem_app/utils/connectivity_utils.dart';
import 'package:canem_app/widgets/blueprints/profile.dart';
import 'package:flutter/material.dart';

class WalkerProfile extends StatefulWidget {
  @override
  _WalkerProfileState createState() => _WalkerProfileState();
}

class _WalkerProfileState extends State<WalkerProfile> {
  Widget defaultWidget = Loading.showLoadingScreen("Your profile is loading");
  bool widgetLoaded = false;

  @override
  Widget build(BuildContext context) {
    if (!widgetLoaded) {
      ConnectivityUtils.executeFunctionIfInternet(() {
        setState(() {
          widgetLoaded = true;
          defaultWidget =
              Profile().buildProfile(context, SessionState.user, "Walker");
        });
      }, () {
        if (!widgetLoaded) {
          setState(() {
            defaultWidget =
                Profile().buildProfile(context, SessionState.user, "Walker");
          });
        }
      });
    }
    return defaultWidget;
  }
}
