import 'package:canem_app/shared/loading.dart';
import 'package:canem_app/shared/session_state.dart';
import 'package:canem_app/utils/pedometer_utils.dart';
import 'package:canem_app/widgets/walker/searchWalkSteps/base.dart';
import 'package:canem_app/widgets/walker/searchWalkSteps/live_walk.dart';
import 'package:canem_app/widgets/walker/searchWalkSteps/searching.dart';
import 'package:flutter/material.dart';

class WalkerSearchWalk extends StatefulWidget {
  WalkerSearchWalk({Key key}) : super(key: key);

  @override
  WalkerSearchWalkState createState() => WalkerSearchWalkState();
}

class WalkerSearchWalkState extends State<WalkerSearchWalk>
    with AutomaticKeepAliveClientMixin {
  static const BASE_WIDGET = 0;
  static const SEARCHING_WIDGET = 1;
  static const LIVE_WALK_WIDGET = 2;

  @override
  bool get wantKeepAlive => true;

  Widget defaultWidget =
      Loading.showLoadingScreen("Your walk history is loading");
  PedometerUtils pedometerUtils;
  Base base;
  Searching searching;
  LiveWalk liveWalk;

  @override
  void initState() {
    super.initState();
    pedometerUtils = PedometerUtils();
    pedometerUtils.setUpPedometer(this);
  }

  @override
  Widget build(BuildContext context) {
    switch (SessionState.walkState) {
      case BASE_WIDGET:
        if (base == null) {
          base = Base();
        }
        defaultWidget = base.baseWidget(this);
        liveWalk = null;
        break;
      case SEARCHING_WIDGET:
        if (searching == null) {
          searching = Searching();
        }
        defaultWidget = searching.searchingWidget(this, pedometerUtils);
        base = null;
        break;
      case LIVE_WALK_WIDGET:
        if (liveWalk == null) {
          liveWalk = LiveWalk();
        }
        defaultWidget = liveWalk.liveWalkWidget(this, pedometerUtils);
        searching = null;
        break;
    }
    return defaultWidget;
  }
}
