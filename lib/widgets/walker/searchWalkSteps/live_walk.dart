import 'dart:async';

import 'package:canem_app/models/walkDTO.dart';
import 'package:canem_app/providers/walk_provider.dart';
import 'package:canem_app/providers/walker_provider.dart';
import 'package:canem_app/providers/walker_walks_provider.dart';
import 'package:canem_app/shared/session_state.dart';
import 'package:canem_app/shared/shared.dart';
import 'package:canem_app/utils/connectivity_utils.dart';
import 'package:canem_app/utils/pedometer_utils.dart';
import 'package:canem_app/widgets/walker/walker_search_walk.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:provider/provider.dart';

class LiveWalk {
  Set<Marker> markers;
  Timer timer;

  Widget liveWalkWidget(WalkerSearchWalkState state, PedometerUtils pedometerUtils) {
    var coords = Shared.parseCoordinates(SessionState.user.identification);
    var lat = coords[0];
    var long = coords[1];
    return SingleChildScrollView(
        child: Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Padding(
            padding: const EdgeInsets.only(top: 20),
            child: Text("Manage your walk",
                style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold))),
        SizedBox(
            width: MediaQuery.of(state.context).size.width,
            height: 400,
            child: Padding(
                padding: const EdgeInsets.all(20),
                child: GoogleMap(
                  onMapCreated: (GoogleMapController controller) {
                    onMapCreated(controller, state);
                  },
                  markers: markers,
                  initialCameraPosition:
                      CameraPosition(target: LatLng(lat, long), zoom: 12),
                ))),
        Text(
          "Your current amount of steps are: \n ${pedometerUtils.steps}",
          textAlign: TextAlign.center,
          overflow: TextOverflow.ellipsis,
          style: TextStyle(fontSize: 15),
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            Padding(
                padding: const EdgeInsets.only(top: 20, bottom: 20),
                child: ElevatedButton(
                    onPressed: () {
                      endWalk(state, pedometerUtils);
                    },
                    child: Text(
                      "End Walk",
                      style: TextStyle(fontSize: 15),
                    ))),
          ],
        ),
      ],
    ));
  }

  void onMapCreated(GoogleMapController controller, WalkerSearchWalkState state) {
    updateMap(controller, state);
    if (timer == null) {
      timer = Timer.periodic(Duration(seconds: 1), (timer) {
        ConnectivityUtils.executeFunctionIfInternet(() {
          Geolocator.getCurrentPosition(desiredAccuracy: LocationAccuracy.high)
              .then((location) {
            SessionState.user.identification = Shared.parseCoordinatesToString(
                location.latitude, location.longitude);
            Provider.of<WalkerProvider>(state.context, listen: false)
                .updateWalker(SessionState.user)
                .then((value) {
              updateMap(controller, state);
            });
          });
        }, () {
          ConnectivityUtils.showConnectivityAlert(
              ConnectivityUtils.defaultTitle,
              ConnectivityUtils.defaultContent,
              state.context);
        });
      });
    }
  }

  void updateMap(GoogleMapController controller, WalkerSearchWalkState state) {
    state.setState(() {
      var coords = Shared.parseCoordinates(SessionState.user.identification);
      var lat = coords[0];
      var long = coords[1];
      markers = new Set<Marker>();
      markers.add(Marker(
          markerId: MarkerId(SessionState.user.id),
          position: LatLng(lat, long)));
      controller.moveCamera(CameraUpdate.newCameraPosition(
          CameraPosition(target: LatLng(lat, long), zoom: 12)));
    });
  }

  void endWalk(State state, PedometerUtils pedometerUtils) async {
    final walkerWalksProvider =
        Provider.of<WalkerWalksProvider>(state.context, listen: false);
    walkerWalksProvider.fetchWalksStatus(1).then((value) {
      Walk walk = walkerWalksProvider.currentWalk;
      if (walk != null) {
        final walkProvider =
            Provider.of<WalkProvider>(state.context, listen: false);
        walk.status = 2;
        if (pedometerUtils.currentSteps != -1) {
          walk.stepsWalked = pedometerUtils.currentSteps;
        } else {
          walk.stepsWalked = 0;
        }
        walkProvider.updateWalk(walk).then((value) {
          if (timer != null) {
            timer.cancel();
            timer = null;
          }
          SessionState.currentWalk = null;
          state.setState(() {
            SessionState.walkState = WalkerSearchWalkState.BASE_WIDGET;
          });
        });
      }
    });
  }
}
