import 'dart:async';

import 'package:canem_app/providers/walk_provider.dart';
import 'package:canem_app/providers/walker_provider.dart';
import 'package:canem_app/providers/walker_walks_provider.dart';
import 'package:canem_app/shared/session_state.dart';
import 'package:canem_app/utils/connectivity_utils.dart';
import 'package:canem_app/utils/pedometer_utils.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../walker_search_walk.dart';

class Searching {
  Timer timer;
  bool activeNotification=false;

  Widget searchingWidget(WalkerSearchWalkState state, PedometerUtils pedometerUtils) {

    fetchingWalks(state, pedometerUtils);
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Text("Looking for a dog to walk",
            style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
            textAlign: TextAlign.center,
            overflow: TextOverflow.ellipsis),
        Container(
            height: 400,
            width: 400,
            alignment: Alignment.center,
            child: CircularProgressIndicator(
              valueColor: AlwaysStoppedAnimation<Color>(Colors.deepPurple),
            )),
        ElevatedButton(
            onPressed: () {
              cancelSearch(state);
            },
            child: Text(
              "Cancel search",
              style: TextStyle(fontSize: 25),
            ),
            style: ElevatedButton.styleFrom(padding: EdgeInsets.all(15))),
      ],
    );
  }

  void fetchingWalks(WalkerSearchWalkState state, PedometerUtils pedometerUtils) async {
    if (timer == null) {
      if (SessionState.walkState == WalkerSearchWalkState.SEARCHING_WIDGET) {
        timer = Timer.periodic(Duration(seconds: 1), (timer) {
          ConnectivityUtils.executeFunctionIfInternet(() {
            activeNotification=false;
            final walkerWalksProvider =
                Provider.of<WalkerWalksProvider>(state.context, listen: false);
            final walkProvider =
                Provider.of<WalkProvider>(state.context, listen: false);
            walkerWalksProvider.fetchWalksStatus(0).then((value) {
              SessionState.currentWalk = walkerWalksProvider.currentWalk;
              if (SessionState.currentWalk != null) {
                SessionState.currentWalk.status = 1;
                walkProvider.updateWalk(SessionState.currentWalk).then((value) {
                  if (this.timer != null) {
                    this.timer.cancel();
                    this.timer = null;
                  }
                  state.setState(() {
                    SessionState.walkState =
                        WalkerSearchWalkState.LIVE_WALK_WIDGET;
                    pedometerUtils.onStepCountRestart();
                  });
                });
              }
            });
          }, () {
            state.setState(() {
              if(!activeNotification){
                ConnectivityUtils.showConnectivityAlert(
                    'An error occurred during the search process',
                    'It looks like your device lost connection. \n Please, check your Internet connection and try again later',
                    state.context);
                activeNotification=true;
                SessionState.walkState = WalkerSearchWalkState.BASE_WIDGET;
              }
            });


          });
        });
      }
    }
  }

  void cancelSearch(WalkerSearchWalkState state) {
    if (timer != null) {
      timer.cancel();
      timer = null;
    }
    SessionState.user.status = false;
    Provider.of<WalkerProvider>(state.context, listen: false)
        .updateWalker(SessionState.user)
        .then((value) {
      state.setState(() {
        SessionState.walkState = WalkerSearchWalkState.BASE_WIDGET;
      });
    });
  }
}
