import 'package:canem_app/providers/walker_provider.dart';
import 'package:canem_app/shared/session_state.dart';
import 'package:canem_app/shared/shared.dart';
import 'package:canem_app/utils/connectivity_utils.dart';
import 'package:canem_app/utils/gps_utils.dart';
import 'package:canem_app/widgets/walker/walker_search_walk.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:geolocator/geolocator.dart';
import 'package:provider/provider.dart';

class Base {
  Widget baseWidget(WalkerSearchWalkState state) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        ElevatedButton(
            onPressed: () {
              startWalk(state);
            },
            child: Text("Start Walking Now", style: TextStyle(fontSize: 30)),
            style: ElevatedButton.styleFrom(padding: EdgeInsets.all(20)))
      ],
    );
  }

  void startWalk(WalkerSearchWalkState state) async {
    Fluttertoast.cancel();
    Fluttertoast.showToast(
        msg: "Please be patient, we are setting up everything for you",
        toastLength: Toast.LENGTH_LONG,
        gravity: ToastGravity.CENTER
    );
    ConnectivityUtils.executeFunctionIfInternet(() {
      GPSUtils.executeFunctionIfGPS(() {
        SessionState.user.status = true;
        Provider.of<WalkerProvider>(state.context, listen: false)
            .updateWalker(SessionState.user);
        SessionState.user.status = false;
        Geolocator.getCurrentPosition(desiredAccuracy: LocationAccuracy.high)
            .then((location) {
          SessionState.user.identification = Shared.parseCoordinatesToString(
              location.latitude, location.longitude);
        state.setState(() {
          SessionState.walkState = WalkerSearchWalkState.SEARCHING_WIDGET;
          });
        });
      }, () {
        state.setState(() {
          ConnectivityUtils.showConnectivityAlert(
              "No GPS avaliable",
              "Your device needs location permissions and location activated in order to search for walkers near your location",
              state.context);
        });
      });
    }, () {
      ConnectivityUtils.showConnectivityAlert(ConnectivityUtils.defaultTitle,
          ConnectivityUtils.defaultContent, state.context);
    });
  }
}
